package com.xl.selenium.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	
	private Properties properties;
	private final String propertyFilePath = "src//main//resources//application.properties";
	
	public ConfigFileReader() {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}		
	}
	
	public String getURL() {
		return properties.getProperty("url.target");
	}
	
	public String getUsername() {
		return properties.getProperty("login.username");
	}
	
	public String getPassword() {
		return properties.getProperty("login.password");
	}
	
	public String getUsername2() {
		return properties.getProperty("login.username2");
	}
	
	public String getPassword2() {
		return properties.getProperty("login.password2");
	}

}
