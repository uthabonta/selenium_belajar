package com.xl.selenium.utility;

import org.testng.annotations.BeforeTest;

import com.xl.selenium.config.BrowserGetter;
import com.xl.selenium.config.ConfigFileReader;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class DriverGetter {
	
	protected WebDriver webDriver;
	protected BrowserGetter browser = new BrowserGetter();
	protected ConfigFileReader configFileReader = new ConfigFileReader();
	
	@BeforeTest
	public void beforeTest() {
		webDriver = browser.getChromeDriver();
		System.out.println("Opening Chrome Drivers");
	}

	@AfterTest
	public void afterTest() {
//		webDriver.close();
//		webDriver.quit();
		System.out.println("Closing Chrome Drivers");
	}

}
