package com.xl.selenium.component;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepBeliStok {
	private WebDriverWait wait;
	private int pulsaAwal;
	private int pulsaAkhir;
	private String packageName;
	
	public int checkBalanceBefore(WebDriver webDriver) {
		wait = new WebDriverWait(webDriver, 30);
		WebElement pulsaBefore = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("header_balance")));
		pulsaAwal = Integer.parseInt(pulsaBefore.getText().replaceAll("[.]*", ""));
		System.out.println("Pulsa awal:: " + pulsaAwal);
		return pulsaAwal;
	}
	
	public String choosePackage(WebDriver webDriver) {
		//webDriver.findElement(By.id("grosirV2-tab")).click();
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		executor.executeScript("arguments[0].click();", webDriver.findElement(By.xpath("//div[text() = 'Rp. 1']")));
		packageName = webDriver.findElement(By.xpath("//div[@class='namaPaket2']")).getText();
		System.out.println("Paket yang dipilih:: " + packageName);
		webDriver.findElement(By.id("Beli")).click();
		return packageName;
	}
	
	public void confirmationBuy(WebDriver webDriver) {
		webDriver.findElement(By.id("btn_ok")).click();
	}
	
	public String getTransactionIdandOk(WebDriver webDriver) {
		wait = new WebDriverWait(webDriver, 30);
		String trxId = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("msg_buy_notif"))).getText();
		System.out.println("Transaksi ID:: " + trxId);
		webDriver.findElement(By.id("btn_buy_notif_ok")).click();
		return trxId;
	}
	
	public int checkBalanceAfter(WebDriver webDriver) {
		wait = new WebDriverWait(webDriver, 30);
		WebElement pulsaAfter = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("header_balance")));
		pulsaAkhir = Integer.parseInt(pulsaAfter.getText().replaceAll("[.]*", ""));
		System.out.println("Pulsa akhir:: " + pulsaAkhir);
		return pulsaAkhir;
	}

}
