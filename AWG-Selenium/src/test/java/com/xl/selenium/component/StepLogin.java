package com.xl.selenium.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.xl.selenium.config.ConfigFileReader;

public class StepLogin {
	
	private ConfigFileReader configFileReader = new ConfigFileReader();
	
	public void doLogin(WebDriver driver) throws InterruptedException {
		driver.get(configFileReader.getURL());
		driver.findElement(By.name("username")).sendKeys(configFileReader.getUsername());
		driver.findElement(By.name("password")).sendKeys(configFileReader.getPassword());
		driver.findElement(By.id("disclaimer")).click();
		Thread.sleep(7000);
		driver.findElement(By.id("btn_submit")).click();
		System.out.println("Sukses Login");
	}

}
