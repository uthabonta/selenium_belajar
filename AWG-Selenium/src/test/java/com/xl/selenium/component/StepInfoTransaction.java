package com.xl.selenium.component;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepInfoTransaction {
	private WebDriverWait wait;
	
	public void checkInfoTrxBeliGrosir(WebDriver webDriver, String packageName) {
		wait = new WebDriverWait(webDriver, 30);
		webDriver.findElement(By.id("transaksi-tab")).click();
		sortByTransactionId(webDriver);
		WebElement bucketId = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='transaksi']//tr[1]//td[8]")));
		System.out.println("Bucket ID:: " + bucketId.getText());
		WebElement namaPaketInfoTrx = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@id='transaksi']//tr[1]//td[3]")));
		System.out.println("Nama Paket Sesuai = " + namaPaketInfoTrx.getText() == packageName);
	}
	
	public void sortByTransactionId(WebDriver webDriver) {
		webDriver.findElement(By.xpath(" //th[contains(text(),'Id Transaksi')]")).click();
		webDriver.findElement(By.xpath("//th[@class='sorting_asc']")).click();
	}

}
