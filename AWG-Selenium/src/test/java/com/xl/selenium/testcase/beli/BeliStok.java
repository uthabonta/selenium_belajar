package com.xl.selenium.testcase.beli;

import org.testng.annotations.Test;

import com.xl.selenium.component.StepBeliStok;
import com.xl.selenium.component.StepInfoTransaction;
import com.xl.selenium.component.StepLogin;
import com.xl.selenium.utility.DriverGetter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

public class BeliStok extends DriverGetter {
	private int pulsaAwal;
	private int pulsaAkhir;
	private String packageName;
	private String trxId;
	
	StepLogin stepLogin = new StepLogin();
	StepBeliStok stepBeliStok = new StepBeliStok();
	StepInfoTransaction stepInfoTrx = new StepInfoTransaction();

	@BeforeClass
	public void beforeClass() throws InterruptedException {
		System.out.println("----- Beli Stok Test will be Executed -----");
		stepLogin.doLogin(webDriver);
	}
	
	@BeforeMethod
	public void beforeMethod() throws InterruptedException {
		Thread.sleep(2000);
	}
	
	@Test(priority = 1)
	public void checkBalanceBefore() {
		pulsaAwal = stepBeliStok.checkBalanceBefore(webDriver);
	}
	
	@Test(priority = 2)
	public void choosePackage() {
		packageName = stepBeliStok.choosePackage(webDriver);
	}
	
	@Test(priority = 3)
	public void confirmationBuy() {
		stepBeliStok.confirmationBuy(webDriver);
	}
	
	@Test(priority = 4)
	public void getTransactionIdandOk() {
		trxId = stepBeliStok.getTransactionIdandOk(webDriver);
	}
	
	@Test(priority = 5)
	public void checkBalanceAfter() {
		pulsaAkhir = stepBeliStok.checkBalanceAfter(webDriver);
	}
	
	@Test(priority = 6)
	public void checkInfoTrxBeliGrosir() {
		stepInfoTrx.checkInfoTrxBeliGrosir(webDriver, packageName);
	}
	
	@AfterMethod
	public void afterMethod(ITestResult result) {
		System.out.println("Anda baru saja melakukan step:: " + result.getMethod().getMethodName());
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Total Pulsa Terpotong:: Rp. " + (pulsaAwal - pulsaAkhir));
		System.out.println("----- Beli Stok Test is Finish -----");
	}

}
