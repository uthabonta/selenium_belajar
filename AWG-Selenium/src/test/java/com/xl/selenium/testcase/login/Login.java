package com.xl.selenium.testcase.login;

import org.testng.annotations.Test;

import com.xl.selenium.component.StepLogin;
import com.xl.selenium.utility.DriverGetter;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class Login extends DriverGetter {

	StepLogin loginStep = new StepLogin();

	@BeforeClass
	public void beforeClass() {
		System.out.println("Login Test will be Executed");
	}

	@Test
	public void loginTest() throws InterruptedException {
		loginStep.doLogin(webDriver);
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Login Test is Finish");
	}

}
