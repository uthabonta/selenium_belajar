package common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Validasi_InfoTrx extends Login_Suite{
	
	DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	LocalDateTime now = LocalDateTime.now();
	public String bucketId2 = null;
	String val_namaPaket = null;
	
	@BeforeTest
	public void a() {
		System.setProperty("webdriver.chrome.driver",webdriverPath);
		options.addArguments("--start-maximized");
	}
	
	@Test(priority = 1)
	public void validasiInfoTrx_openTab() throws Exception {
  		System.out.println("\n"+"=== PROSES VALIDASI INFO TRANSAKSI ===");
		driver.findElement(By.id("transaksi-tab")).click();
	}

	@Test(priority = 2)
	public void validasiInfoTrx_startDate() {
		//driver.findElement(By.id("date")).sendKeys(date.format(now));
		driver.findElement(By.id("date")).sendKeys(date.format(now));
	}
  
  
	@Test(priority = 3)
	public void validasiInfoTrx_endDate() {
		driver.findElement(By.id("date2")).sendKeys(date.format(now));
	}
  
	@Test(priority = 4)
	public void validasiInfoTrx_submitDate() {
		driver.findElement(By.id("form_transaksi")).click();
	}
  
	@Test(priority = 5)
	public void validasiInfoTrx_getData() {
	  WebDriverWait wait = new WebDriverWait(driver, 10);
	//ambil bucketID
	  WebElement bucket =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
	  bucket = wait.until(ExpectedConditions.visibilityOf(bucket));
	  bucketId2 = bucket.getText();
	  System.out.println("BucketId2 = "+bucketId2);
	
	//ambil nama paket
	WebElement namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]"));
	namaPaketInfoTrx = wait.until(ExpectedConditions.visibilityOf(namaPaketInfoTrx));
	val_namaPaket = namaPaketInfoTrx.getText();
	System.out.println("Nama Paket di Info Transaksi = "+val_namaPaket);
	}
}
