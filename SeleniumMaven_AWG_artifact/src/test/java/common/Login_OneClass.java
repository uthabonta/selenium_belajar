package common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class Login_OneClass {

	String user = "sanity_testing";
	public static String nomuser = "6283805278034";
	String pass = "Axis12345678*";
	
	String user2 = "jendraljoko";
	public static String nomuser2 = "6283873191582";
	String pass2 = "Password*10";
	
	String user3 = "komandan_test_nat";
	public static String nomuser3 = "6283892789602";
	String pass3 = "Axis12345678*";
	
	
	String url = "https://grosir.axisnet.id";
	String urlNow = null;
	String messageWrongInput = null;
	String testcase_result = null;
	
	public void loginStep(WebDriver driver) throws InterruptedException {

		driver.get(url);
		System.out.println("=== PROSES LOGIN ===");
		driver.findElement(By.name("username")).sendKeys(user2);
		  System.out.println("berhasil input username");
		  driver.findElement(By.name("password")).sendKeys(pass2);
		  System.out.println("berhasil input password");
		  driver.findElement(By.id("disclaimer")).click();
		  System.out.println("berhasil klik checkbox");
		  Thread.sleep(7000);
		  driver.findElement(By.id("btn_submit")).click();
		  System.out.println("berhasil klik submit");
		  
		  System.out.println("");
		  System.out.println("=== STATUS LOGIN ===");
		  String expectedUrl = "https://grosir.axisnet.id/index";
		  urlNow = driver.getCurrentUrl();
		  try {
			  messageWrongInput = driver.findElement(By.id("msg_p")).getText();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Tidak ada error");
		}
		  
		  if (urlNow.equals(expectedUrl)) {
			  testcase_result = "SUKSES";
			  System.out.println("Login "+testcase_result+"\n");
		}else {
			testcase_result = "GAGAL";
			System.out.println("Login "+testcase_result);
			System.out.println("Error output frontend : "+messageWrongInput+"\n");
			System.out.println("");
		}
	}

}
