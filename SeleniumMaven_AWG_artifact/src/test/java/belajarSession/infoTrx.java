package belajarSession;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.util.List;

public class infoTrx extends GlobalProperties{
	
	public static void klikMenuInfoTransaksi() {
		driver.findElement(By.id("transaksi-tab")).click();
	}
	
	public static void pilihJenisTransaksi(String jenisTransaksi) throws InterruptedException {
	Thread.sleep(1000);
	Select pilihTipe = new Select(driver.findElement(By.xpath("//select[@id='transaksiInfo']")));
	List<WebElement> listTipe = pilihTipe.getOptions();
	int jmlTipe = listTipe.size();
	
//	pilihPaket.selectByIndex(53);
	pilihTipe.selectByVisibleText(jenisTransaksi);
//	AIGO - Nelpon Sepuasnya ke sesama AXIS 14hr Exp by 31 Oct 2019 (6) -- Beli Rp 1
	System.out.println("sizing paket = "+jmlTipe);
	}
	
	public static void aturTanggal(LocalDateTime tanggal) {
		driver.findElement(By.id("date")).sendKeys(date.format(tanggal));
		driver.findElement(By.id("date2")).sendKeys(date.format(tanggal));
		driver.findElement(By.id("form_transaksi")).click();
	}
	
	public static void ambilDataBeliGrosir() {
		  WebDriverWait wait = new WebDriverWait(driver, 10);
		//ambil bucketID
		  WebElement bucket =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
		  bucket = wait.until(ExpectedConditions.visibilityOf(bucket));
		  bucketId2 = bucket.getText();
		  System.out.println("BucketId2 = "+bucketId2);
		
		//ambil nama paket
		WebElement namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]"));
		namaPaketInfoTrx = wait.until(ExpectedConditions.visibilityOf(namaPaketInfoTrx));
		val_namaPaket = namaPaketInfoTrx.getText();
		System.out.println("Nama Paket di Info Transaksi = "+val_namaPaket);
		
	}
  
}
