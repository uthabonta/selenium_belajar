package belajarSession;

import java.util.concurrent.TimeUnit;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class exe_login extends GlobalProperties {
	
	@BeforeTest
	public void openBrowser() {
		GlobalProperties.openDriver();
		GlobalProperties.masukKeUrl();
	}
	
	@BeforeMethod
	public void tunggu() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	public void login() throws InterruptedException{
		login.masukkanUsername(user2);
		login.masukkanPassword(pass2);
		login.klikCekBox();
		login.isiCaptcha();
		login.klikMasuk();
		login.validasiLogin();
	}
	
	@Test(priority = 2)
	public void infoTransaksi() throws Exception {
		infoTrx.klikMenuInfoTransaksi();
		infoTrx.pilihJenisTransaksi(jenisTrx_pv);
		infoTrx.aturTanggal(now);
		
	}
	
	@AfterMethod
    public void afterMethod(ITestResult result) {
      System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
    	}
}
