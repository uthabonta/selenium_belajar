package belajarSession;

import java.util.concurrent.TimeUnit;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class exe_beli extends GlobalProperties {
	
	@BeforeTest
	public void openBrowser() {
		GlobalProperties.openDriver();
		GlobalProperties.masukKeUrl();
	}
	
	@BeforeMethod
	public void tunggu() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	public void loginAWG() throws InterruptedException{
		login.masukkanUsername(user2);
		login.masukkanPassword(pass2);
		login.klikCekBox();
		login.isiCaptcha();
		login.klikMasuk();
		login.validasiLogin();
	}
	
	@Test(priority = 2)
	public void beliGrosir() throws InterruptedException {
		beli.klikMenuBeli();
		beli.checkLastBalance();
		beli.pilihPaket(paket1rupiah);
		beli.klikBeli();
		beli.ambilTrxId();
		beli.klikOkAfterBuy();
		
	}
	
	@Test(priority = 3)
	public void infoTransaksi() throws InterruptedException {
		infoTrx.klikMenuInfoTransaksi();
		infoTrx.pilihJenisTransaksi(jenisTrx_pv);
		infoTrx.aturTanggal(now);
		
	}
	
	@AfterMethod
    public void afterMethod(ITestResult result) {
      System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
    	}
}
