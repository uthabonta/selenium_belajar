package belajarSession;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testCase.Beli.Beli_PulsaCukup;

public class beli extends GlobalProperties{
	
	public static void klikMenuBeli() {
		driver.findElement(By.id("grosirV2-tab")).click();
	}
	
	public static void pilihPaket(String paketDipilih) {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement klikpaket = driver.findElement(By.xpath("//div[text() = '"+paketDipilih+"']"));
		executor.executeScript("arguments[0].click();", klikpaket);
		namaPaket = driver.findElement(By.xpath("//div[@class='namaPaket2']")).getText();
		System.out.println("Nama paket yang dipilih = "+namaPaket);
	}
	
	public static void checkLastBalance() {
		  WebDriverWait wait = new WebDriverWait(driver, 5);
		  WebElement Pulsa = driver.findElement(By.id("header_balance"));
		  Pulsa = wait.until(ExpectedConditions.visibilityOf(Pulsa));
		  String PulsaAwal =  Pulsa.getText().replaceAll("[.]*", "");
		  val_PulsaAwal = Integer.parseInt(PulsaAwal);
		  System.out.println("valpulsa awal = "+val_PulsaAwal);
	}
	
	public static void klikBeli() {
  		driver.findElement(By.id("Beli")).click();
	}
	
	public static void klikOkConfirmation() {
  		driver.findElement(By.id("btn_ok")).click();
	}
	
	public static void ambilTrxId() {
  		WebDriverWait wait = new WebDriverWait(driver, 10);
  		WebElement trxIdSpan =  driver.findElement(By.id("msg_buy_notif"));
  		String trxId = wait.until(ExpectedConditions.visibilityOf(trxIdSpan)).getText();
  		System.out.println(trxId);
  			//String text=trxId;
  			Beli_PulsaCukup trimTrxId = new Beli_PulsaCukup();
  			trimTrxId.getTextAfter(trxId);
	}
	
	public void getTextAfter(String text) {
			String [] input = text.split(" ");
			int lastIndex = input.length;
			String lastText=(input[lastIndex-1]);
			bucketId1 = lastText.substring(1, 8);
			System.out.println("BucketId = "+bucketId1);
			
		}
	
	public static void klikOkAfterBuy(){
 		 driver.findElement(By.id("btn_buy_notif_ok")).click();
	}
}
