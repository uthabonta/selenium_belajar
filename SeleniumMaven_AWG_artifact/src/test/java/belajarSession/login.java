package belajarSession;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class login extends GlobalProperties{
	static String user = "sanity_testing";
	public static String nomuser = "6283805278034";
	static String pass = "Axis12345678*";
	
	String user2 = "jendraljoko";
	public static String nomuser2 = "6283873191582";
	String pass2 = "Password*10";
	
	String user3 = "komandan_test_nat";
	public static String nomuser3 = "6283892789602";
	String pass3 = "Axis12345678*";
	
	public static void masukkanUsername(String username) {
		GlobalProperties.printStep(new Object() {}.getClass().getEnclosingMethod().getName());
		driver.findElement(By.name("username")).sendKeys(username);
	}	  
	
	
	public static void masukkanPassword(String password) {
		GlobalProperties.printStep(new Object() {}.getClass().getEnclosingMethod().getName());
		  driver.findElement(By.name("password")).sendKeys(password);
	}
	
	public static void klikCekBox() {
		GlobalProperties.printStep(new Object() {}.getClass().getEnclosingMethod().getName());
		  driver.findElement(By.id("disclaimer")).click();
	}
	
	public static void isiCaptcha() throws InterruptedException {
		GlobalProperties.printStep(new Object() {}.getClass().getEnclosingMethod().getName());
		Thread.sleep(7000);
	}
	
	public static void klikMasuk() {
		GlobalProperties.printStep(new Object() {}.getClass().getEnclosingMethod().getName());
		driver.findElement(By.id("btn_submit")).click();
	}
	
	public static void validasiLogin() {
		 System.out.println("");
		  System.out.println("=== STATUS LOGIN ===");
		  String expectedUrl = "https://grosir.axisnet.id/index";
		  urlNow = driver.getCurrentUrl();
		  try {
			  messageWrongInput = driver.findElement(By.id("msg_p")).getText();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Tidak ada error");
		}
		  
		  if (urlNow.equals(expectedUrl)) {
			  login_result = "SUKSES";
			  System.out.println("Login "+login_result+"\n");
		}else {
			login_result = "GAGAL";
			System.out.println("Login "+login_result);
			System.out.println("Error output frontend : "+messageWrongInput+"\n");
			System.out.println("");
		}
	}
}
