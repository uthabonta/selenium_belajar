package belajarSession;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class GlobalProperties {
	
	public static String user = "sanity_testing";
	public static String nomuser = "6283805278034";
	static String pass = "Axis12345678*";
	
	public String user2 = "jendraljoko";
	public static String nomuser2 = "6283873191582";
	public String pass2 = "Password*10";
	
	public static WebDriver driver;
	static String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
	static ChromeOptions options = new ChromeOptions();
	
	public static String url = "https://grosir.axisnet.id";
	public static String urlNow = null;
	public static String messageWrongInput = null;
	public static String login_result = null;
	
	public static String random = RandomStringUtils.randomAlphanumeric(8);
	
	public static DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	public static DateTimeFormatter timeWithSecond = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	public static LocalDateTime now = LocalDateTime.now();
	
	public static String reason = null;
	public static String namaPaket = null;
	public static String val_namaPaket = null;
	public static int val_PulsaAwal = 0;
	public static int val_PulsaAkhir = 0;
	
	
	public static String bucketId1 = null;
	public static String bucketId2 = null;
	public static String expiredDate = null;
	
	public static String paket1rupiah = "Rp. 22.700";
	
	public static String jenisTrx_pv = "Physical Voucher";

	

	
	public static void openDriver() {
		System.setProperty("webdriver.chrome.driver",webdriverPath);
		options.addArguments("--start-maximized");
		driver = new ChromeDriver(options);

	}
	
	public static void printStep(String stepSaatIni) {
		System.out.println("Saat ini sedang = "+stepSaatIni);
	}
	
	public static void masukKeUrl(){
		driver.get(url);
	}
}
