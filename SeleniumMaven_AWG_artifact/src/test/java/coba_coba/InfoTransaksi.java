package coba_coba;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class InfoTransaksi {
	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
//	String webdriverPath = "/home/adew/UTHA/selenium/geckodriver-v0.25.0-linux64/geckodriver";
	String url = "https://grosir.axisnet.id";
	public WebDriver driver;
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	String user = "jendraljoko";
	String pass = "Password*10";
	String divLoginError = "";
	
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
    LocalDateTime now = LocalDateTime.now();
    
    ChromeOptions options = new ChromeOptions();
 

  @BeforeTest
  public void openBrowser() throws InterruptedException {
//	  System.setProperty("webdriver.gecko.driver",webdriverPath);
//	  driver = new FirefoxDriver();
//	  options.addArguments("--no-sandbox"); //Bypass OS security model   
//	  options.addArguments("--start-maximized");
////	  options.addArguments("--disable-dev-shm-usage");
//	  options.addArguments("--headless");
	  System.setProperty("webdriver.chrome.driver",webdriverPath);
	  driver = new ChromeDriver();
	  



	  
	  driver.get(url);
	  driver.findElement(By.name("username")).sendKeys(user);
	  System.out.println("berhasil input username");
	  driver.findElement(By.name("password")).sendKeys(pass);
	  System.out.println("berhasil input password");
	  driver.findElement(By.id("disclaimer")).click();
	  System.out.println("berhasil klik checkbox");
	  Thread.sleep(7000);
	  driver.findElement(By.id("btn_submit")).click();
	  System.out.println("berhasil klik submit");
	  //wait = new WebDriverWait(driver, 2);
		//driver.switchTo().alert().accept();
	 // System.out.println("berhasil masuk");
	  
//	  divLoginError = driver.findElement(By.id("msg_p")).getText();
//	  if (divLoginError!="") {
//		System.out.println("Warning!! Ada error ketika login");
//		System.out.println("error ketika login = "+divLoginError);
//	} else {
//		System.out.println("login aman");
//	}
//	  
	}
  
  		
  @BeforeMethod
  public void beforeMethod() throws InterruptedException {
	  Thread.sleep(3000);
  }
	  
  @Test(priority = 0)
  public void clickTabInfoTransaksi() throws InterruptedException {
	  Thread.sleep(5000);
	  driver.findElement(By.id("transaksi-tab")).click();
  }
  
  @Test(priority = 1)
  private void checkCodeBuy() {
	String nilai = driver.findElement(By.xpath("//tr[@class='odd']//td[contains(text(),'2019-10-07')]")).getText();
	////tr[@class='odd']//td[contains(text(),'2019-10-07')]
	System.out.println(nilai);
	}
  
  @Test(priority = 2)
  private void clickStartDate() {
	driver.findElement(By.id("date")).sendKeys(dtf.format(now));	
	}
  
  
  @Test(priority = 3)
  private void clickEndDate() {
	driver.findElement(By.id("date2")).sendKeys(dtf.format(now));
  	}

  @Test(priority = 4)
  private void submitDate() {
	driver.findElement(By.id("form_transaksi")).click();
	}
  
  
  
  


  @AfterMethod
  public void afterMethod() throws InterruptedException{
	  Thread.sleep(2000);
  }



  @AfterTest
  public void closeBrowser() {
	  //driver.close();
  }
}
