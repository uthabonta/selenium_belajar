package coba_coba;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class BeliPaket {
	
//  	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
	String webdriverPath = "/home/adew/UTHA/selenium/geckodriver-v0.25.0-linux64/geckodriver";
	String url = "https://grosir.axisnet.id";
	public WebDriver driver;
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	String user = "jendraljoko";
	String pass = "Password*10";
	String divLoginError = "";
	
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
    LocalDateTime now = LocalDateTime.now();
    
	  @BeforeTest
	  public void openBrowser() throws InterruptedException {
		  System.setProperty("webdriver.gecko.driver",webdriverPath);
		  driver = new FirefoxDriver();
		  
//		  System.setProperty("webdriver.chrome.driver",webdriverPath);
//		  driver = new ChromeDriver();
		  
		  
		  driver.get(url);
		  driver.findElement(By.name("username")).sendKeys(user);
		  System.out.println("berhasil input username");
		  driver.findElement(By.name("password")).sendKeys(pass);
		  System.out.println("berhasil input password");
		  driver.findElement(By.id("disclaimer")).click();
		  System.out.println("berhasil klik checkbox");
		  Thread.sleep(7000);
		  driver.findElement(By.id("btn_submit")).click();
		  System.out.println("berhasil klik submit");
	  }
	  
	  @BeforeMethod
	  public void beforeMethod() throws InterruptedException {
		  Thread.sleep(2000);
	  }
	  
	  @Test(priority = 0)
	  public void checkLastBalance() {
		  WebElement PulsaAwal = driver.findElement(By.id("header_balance"));
		  System.out.println("pulsa awal = "+PulsaAwal.getText());
	  }
	  
	  @Test(priority = 1)
	  public void clickTabBuyPackage() {
		 driver.findElement(By.id("grosirV2-tab")).click();
	  }
	  
	  @Test(priority = 2)
	  public void choosePackage() {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement klikpaket = driver.findElement(By.xpath("//div[text() = 'Rp. 1']"));
		executor.executeScript("arguments[0].click();", klikpaket);
		 
	  }
	  
	  @Test(priority = 3)
		public void clickBeli() {
	  		driver.findElement(By.id("Beli")).click();
		}
	  
	  	@Test(priority = 4)
		public void clickOk() {
	  		driver.findElement(By.id("btn_ok")).click();
		}
	  	
	  	@Test(priority = 4)
		public void clickOKConfirmation() {
	  		driver.findElement(By.id("btn_buy_notif_ok")).click();
		}
	  	
	  	@Test(priority = 5)
		public void testName(){
	  		WebElement PulsaAkhir = driver.findElement(By.id("header_balance"));
	  		System.out.println("Pulsa Akhir = "+PulsaAkhir.getText());
		}
	  	
	  @AfterMethod
	  public void afterMethod() throws InterruptedException{
		  Thread.sleep(2000);
		  System.out.println();
	  }
	
	 
	  @AfterTest
	  public void afterTest() {
	  }

}
