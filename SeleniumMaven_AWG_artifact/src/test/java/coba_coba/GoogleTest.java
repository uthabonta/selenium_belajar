package coba_coba;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.awt.RenderingHints.Key;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class GoogleTest {
	
	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
	String url = "https://google.com";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
    LocalDateTime now = LocalDateTime.now();
    public WebDriver driver;
    ChromeOptions options = new ChromeOptions();
    JavascriptExecutor executor = (JavascriptExecutor)driver;
    String title="";
    String urlNow="";
    String inputKetik ="Halo gaeszzs";
	
  @BeforeTest
  public void beforeTest() {
	  
	  options.addArguments("--no-sandbox"); //Bypass OS security model   
	  options.addArguments("--start-maximized");
	  options.addArguments("--headless");
	  System.setProperty("webdriver.chrome.driver",webdriverPath);
	  driver = new ChromeDriver(options);
	  driver.get(url);
	  System.out.println("open browser");
	  
	  }
	  
	  
	  @BeforeMethod
  public void beforeMethod() throws InterruptedException {
	Thread.sleep(2000);
	  title =  driver.getTitle();
	  System.out.println("Title = "+title);
	
  
  }
  
  @Test(priority = 0)
  public void searchBox() {
	  driver.findElement(By.name("q")).sendKeys(inputKetik);
	  System.out.println("berhasil ketik");
  }
  
  @Test(priority = 1)
  public void clickSearch() {
	  driver.findElement(By.name("btnK")).click();
  }
  

  @AfterMethod
  public void afterMethod() throws InterruptedException {
	  urlNow =  driver.getCurrentUrl();
	  System.out.println("Url sekarang = "+urlNow);
	  Thread.sleep(2000);
  }

  

  @AfterTest
  public void afterTest() throws InterruptedException {
	  Thread.sleep(2000);
	  driver.close();
	  System.out.println("close browser");
  }

}
