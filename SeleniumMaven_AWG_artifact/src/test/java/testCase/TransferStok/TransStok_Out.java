package testCase.TransferStok;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.GlobalProperties;
import common.Login_OneClass;
import common.Login_Suite;

public class TransStok_Out extends Login_Suite {

	//id=stok-tab klik
	
	//div[@id='table-stok_filter']//input[@class='form-control input-sm']
	//masukin bucketId
	
	//get td[4] expired date
	//get td[2] harga beli
	
	//id transferStock-tab
	//id=HPnomor
	//ul[@id='viewPacketTransfer']//div[text() = 'expired date (sudah ditambahi minus)']
	//ul[@id='viewPacketTransfer']//div[text() = '05-Nov-2019']
	
	//ul[@id='carts']//div[text() = 'nama paket']
	// id =kirim
	
	// id = msg_conf_trans_notif >> get notif
	//get database otp
	//id = otp-transfer >> send key
	//id = captcha-transfer >> send key
	//id = btn_ok_trans >> klik
	String className = this.getClass().getSimpleName();
	
	String testcase_result = null;
	int int_testcase_result = 0;
	
	String reason = null;
	String namaPaket = null;
	String val_namaPaket = null;
	int val_PulsaAwal = 0;
	int val_PulsaAkhir = 0;
	
	
	String bucketId1 = null;
	String bucketId2 = null;
	String expiredDate = null;
//	
//	public static WebDriver driver = null;
//	ChromeOptions options = new ChromeOptions();
//	
//	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
	DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	DateTimeFormatter timeWithSecond = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    
//	@BeforeTest
//	  public void LoginAWG() throws InterruptedException{
//		  System.setProperty("webdriver.chrome.driver",webdriverPath);
//		  options.addArguments("--start-maximized");
//		  driver = new ChromeDriver(options);
//		  Login_OneClass loginAWGFirst = new Login_OneClass();
//		  System.out.println("Automation Testing untuk "+className+" DIMULAI ... "+timeWithSecond.format(now)+"\n");
//		  loginAWGFirst.loginStep(driver);
//	  }

	  
	@BeforeMethod
	public void beforeMethod() throws InterruptedException{
	  Thread.sleep(2000);
		}
	
	@AfterMethod
    public void afterMethod(ITestResult result) {
      System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
    	}
	
//	@Test(priority = 1)
//	public void validasiInfoTrx_openTab() throws Exception {
//  		System.out.println("\n"+"=== PROSES VALIDASI INFO TRANSAKSI ===");
//		driver.findElement(By.id("transaksi-tab")).click();
//	}
//
//	@Test(priority = 2)
//	public void validasiInfoTrx_startDate() {
//		//driver.findElement(By.id("date")).sendKeys(date.format(now));
//		driver.findElement(By.id("date")).sendKeys(date.format(now));
//	}
//  
//  
//	@Test(priority = 3)
//	public void validasiInfoTrx_endDate() {
//		driver.findElement(By.id("date2")).sendKeys(date.format(now));
//	}
//  
//	@Test(priority = 4)
//	public void validasiInfoTrx_submitDate() {
//		driver.findElement(By.id("form_transaksi")).click();
//	}
//  
//	@Test(priority = 5)
//	public void validasiInfoTrx_getData() {
//	  WebDriverWait wait = new WebDriverWait(driver, 10);
//	//ambil bucketID
//	  WebElement bucket =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
//	  bucket = wait.until(ExpectedConditions.visibilityOf(bucket));
//	  bucketId2 = bucket.getText();
//	  System.out.println("BucketId2 = "+bucketId2);
//	
//	//ambil nama paket
//	WebElement namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]"));
//	namaPaketInfoTrx = wait.until(ExpectedConditions.visibilityOf(namaPaketInfoTrx));
//	val_namaPaket = namaPaketInfoTrx.getText();
//	System.out.println("Nama Paket di Info Transaksi = "+val_namaPaket);
//	}
	
//	@Test (priority = 6)
//	public void bukaMenuStok() {
//		driver.findElement(By.id("stok-tab")).click();
//	}
//	
//	@Test (priority = 7)
//	public void searchBucketId() throws InterruptedException{
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//input[@type='search']")).sendKeys(bucketId2);
//	}
//	
//	@Test (priority = 8)
//	public void getExpiredDate() {
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		expiredDate = driver.findElement(By.xpath("//td[4]")).getText().replace(" ", "-");
//		//expiredDate = new StringBuilder(expiredDate).insert(expiredDate.length()-4, "-").toString();
//		
//		int sisaStokSaatIni = Integer.valueOf(driver.findElement(By.xpath("//td[6]")).getText());
//		System.out.println("Sisa stok = "+sisaStokSaatIni);
//		System.out.println("Expired date = "+expiredDate);
//	}
	
	@Test (priority = 9)
	public void bukaMenuTransStok() {
		driver.findElement(By.id("transferStock-tab")).click();
	}
	
	@Test (priority = 10)
	public void inputNomor() {
		driver.findElement(By.id("HPnomor")).sendKeys(Login_OneClass.nomuser3);
	}
	
	@Test (priority = 11)
	public void pilihPaketByExpired(){
		driver.findElement(By.xpath("//ul[@id='viewPacketTransfer']//div[text() = '"+expiredDate+"']")).click();
	}
	
	@Test (priority = 12)
	public void klikKirim() throws InterruptedException{
		driver.findElement(By.id("kirim")).click();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(10000);
	}
	
	@Test (priority = 13)
	public void klikOkTransfer() throws Exception {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement klikOk = driver.findElement(By.id("btn_ok_trans"));
		executor.executeScript("arguments[0].click();", klikOk);
	}
	
	 @Test (priority = 14)
		public void validasi_bukaMenuStok() {
			driver.findElement(By.id("stok-tab")).click();
		}
		
		@Test (priority = 15)
		public void validasi_searchBucketId() throws InterruptedException{
			Thread.sleep(5000);
			driver.findElement(By.xpath("//input[@type='search']")).sendKeys(bucketId2);
		}
		
		@Test (priority = 16)
		public void validasi_getSisaStok() {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			int sisaStokSaatIni = Integer.valueOf(driver.findElement(By.xpath("//td[6]")).getText());
			System.out.println("Sisa stok = "+sisaStokSaatIni);
		}
		 
  	@AfterTest 
    public void validasiAfterTest() {
  		System.out.println("");
  		if (int_testcase_result == 2) {
  			testcase_result = "SUKSES";
		}else {
			testcase_result = "GAGAL";
		}
  		
//	  		System.out.println(val_PulsaAwal);
//	  		System.out.println(val_PulsaAkhir);
//	  		System.out.println(namaPaket);
//	  		System.out.println(val_namaPaket);
  		
  		System.out.println(int_testcase_result);
  		System.out.println("Automation Testing untuk "+className+" SELESAI ... "+timeWithSecond.format(now)+"\n");
		  System.out.println("=== HASIL AKHIR ===");
		  System.out.println("Skenario "+className+" = "+testcase_result+reason+"\n");
    }
}
