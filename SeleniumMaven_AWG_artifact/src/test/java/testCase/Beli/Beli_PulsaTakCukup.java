package testCase.Beli;

import org.testng.annotations.Test;

import common.Login_OneClass;

import org.testng.annotations.BeforeMethod;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class Beli_PulsaTakCukup extends Login_OneClass{
String className = this.getClass().getSimpleName();
	
	String testcase_result = null;
	int int_testcase_result = 0;
	
	String reason = null;
	String namaPaket = null;
	String val_namaPaket = null;
	int val_PulsaAwal = 0;
	int val_PulsaAkhir = 0;
	
	
	int hargaDipilih = 0;
	
	
	ChromeOptions options = new ChromeOptions();
	
		
		
		public static WebDriver driver = null;
		String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
		DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter timeWithSecond = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	    LocalDateTime now = LocalDateTime.now();
		

	  @BeforeTest
	  public void LoginAWG() throws InterruptedException{
		  System.setProperty("webdriver.chrome.driver",webdriverPath);
		  options.addArguments("--start-maximized");
		  driver = new ChromeDriver(options);
		  Login_OneClass loginAWGFirst = new Login_OneClass();
		  System.out.println("Automation Testing untuk "+className+" DIMULAI ... "+timeWithSecond.format(now)+"\n");
		  loginAWGFirst.loginStep(driver);
	  }

	  
	  @BeforeMethod
	  public void beforeMethod() throws InterruptedException{
		  Thread.sleep(2000);
	  }
	  
	  @Test(priority = 1)
	  public void checkLastBalance() {
		  System.out.println("=== PROSES BELI ===");
		  WebDriverWait wait = new WebDriverWait(driver, 10);
		  WebElement Pulsa = driver.findElement(By.id("header_balance"));
		  Pulsa = wait.until(ExpectedConditions.visibilityOf(Pulsa));
		  String PulsaAwal =  Pulsa.getText().replaceAll("[.]*", "");
		  val_PulsaAwal = Integer.parseInt(PulsaAwal);
		  System.out.println("Pulsa awal = "+val_PulsaAwal);
		  
		  
	  }
	  
	  @Test(priority = 2)
	  public void clickTabBuyPackage() {
		 driver.findElement(By.id("grosirV2-tab")).click();
	  }
	  
	  @Test(priority = 3)
	  public void choosePackage() {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		//div[@class='curPrice']//div[contains(text(),'Rp. ')]
		//div[text() = 'Rp. 1']
		List<WebElement> listHarga = driver.findElements(By.xpath("//div[@class='curPrice']//div[contains(text(),'Rp. ')]"));
		for (int i = 0; i < listHarga.size(); i++) {
			int intlistHarga = 0;
			intlistHarga = Integer.valueOf(listHarga.get(i).getText().replaceAll("[Rp. ]*", ""));
			//System.out.println("list ke-"+i+" = "+intlistHarga);
			
			if (intlistHarga>hargaDipilih) {
				hargaDipilih = intlistHarga;
				
			}
		}
		String hargaDipilihStr = Integer.toString(hargaDipilih);
		hargaDipilihStr = new StringBuilder(hargaDipilihStr).insert(hargaDipilihStr.length()-3, ".").toString();
		System.out.println("harga dipilih = "+hargaDipilihStr);
		WebElement klikpaket = driver.findElement(By.xpath("//div[@class='curPrice']//div[contains(text(),'Rp. "+hargaDipilihStr+"')]"));
		executor.executeScript("arguments[0].click();", klikpaket);
		namaPaket = driver.findElement(By.xpath("//div[@class='namaPaket2']")).getText();
		System.out.println("Nama paket yang dipilih = "+namaPaket);
		
		 
	  }
	  
	  @Test(priority = 4)
		public void clickBeli() {
	  		driver.findElement(By.id("Beli")).click();
		}
	  
	  	@Test(priority = 5)
		public void clickOk() {
	  		driver.findElement(By.id("btn_ok")).click();
		}
	  	
//	  	@Test(priority = 6)
//		public void getTransactionId() {
//	  		WebDriverWait wait = new WebDriverWait(driver, 10);
//	  		WebElement trxIdSpan =  driver.findElement(By.id("msg_buy_notif"));
//	  		String trxId = wait.until(ExpectedConditions.visibilityOf(trxIdSpan)).getText();
//	  		System.out.println(trxId);
//	  		
//		}
	  	
	  	@Test(priority = 6)
		public void getMsgBuyNotif() {
	  		WebDriverWait wait = new WebDriverWait(driver, 10);
	  		WebElement msgBuyNotif =  driver.findElement(By.id("msg_notif"));
	  		String msgBuyNotif_str = wait.until(ExpectedConditions.visibilityOf(msgBuyNotif)).getText();
	  		System.out.println(msgBuyNotif_str);
	  		
		}
	  	
	  	@Test(priority = 7)
		public void clickOKConfirmation(){
	  		 driver.findElement(By.id("btn_notif_ok")).click();
		}
	  	
	  	@Test(priority = 9)
		public void checkBalanceAfterBuy(){
	  		WebElement Pulsa = driver.findElement(By.id("header_balance"));
	  		String PulsaAkhir = Pulsa.getText().replaceAll("[.]*", "");
	  		val_PulsaAkhir = Integer.valueOf(PulsaAkhir);
	  		System.out.println("Pulsa Akhir = "+val_PulsaAkhir);
		}
	  	
	  	@Test(priority = 8)
		public void validasiInfoTrx_openTab() throws Exception {
	  		System.out.println("\n"+"=== CEK INFO TRANSAKSI ===");
			driver.findElement(By.id("transaksi-tab")).click();
		}

		@Test(priority = 10)
		public void validasiInfoTrx_startDate() {
		driver.findElement(By.id("date")).sendKeys(date.format(now));	
		}
	  
	  
	  @Test(priority = 11)
	  public void validasiInfoTrx_endDate() {
		driver.findElement(By.id("date2")).sendKeys(date.format(now));
	  }
	  
	  @Test(priority = 12)
	  public void validasiInfoTrx_submitDate() {
		driver.findElement(By.id("form_transaksi")).click();
		}
	  
	  @Test(priority = 13)
	  public void validasiInfoTrx_getData() {
		  WebDriverWait wait = new WebDriverWait(driver, 10);
		//ambil bucketID
		  WebElement bucketId =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
		bucketId = wait.until(ExpectedConditions.visibilityOf(bucketId));
		System.out.println("BucketId = "+bucketId.getText());
		
		//ambil nama paket
		WebElement namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]"));
		namaPaketInfoTrx = wait.until(ExpectedConditions.visibilityOf(namaPaketInfoTrx));
		val_namaPaket = namaPaketInfoTrx.getText();
		System.out.println("Nama Paket di Info Transaksi = "+val_namaPaket);
		}
	  
	  @Test(priority = 14)
	  public void validasi_Pulsa() {
		  if (val_PulsaAwal-val_PulsaAkhir==1) {
	  			int_testcase_result = 1;
				reason = ", Pulsa sudah terpotong dan sesuai!";
			
	  		}else {
				int_testcase_result = int_testcase_result+0;
				reason = ", pulsa belum terpotong!";
			}
		}
	  
	  @Test(priority = 15)
	  public void validasi_InfoTrx() {
		  if (val_namaPaket.equals(namaPaket)) {
	  			int_testcase_result = int_testcase_result+1;
				reason = reason+"&"+", Transaksi sudah muncul di Info Transaksi";
			}else {
				int_testcase_result = int_testcase_result+0;
				reason = reason+" & "+", Transaksi tidak muncul di Info Transaksi";
			}
		}
	  	
  	@AfterMethod
    public void afterMethod(ITestResult result) {
      System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
    }
	
	 
  	@AfterTest
    public void validasiAfterTest() {
  		System.out.println("");
  		if (int_testcase_result == 2) {
  			testcase_result = "SUKSES";
		}else {
			testcase_result = "GAGAL";
		}
  		
//  		System.out.println(val_PulsaAwal);
//  		System.out.println(val_PulsaAkhir);
//  		System.out.println(namaPaket);
//  		System.out.println(val_namaPaket);
  		
  		System.out.println(int_testcase_result);
  		System.out.println("Automation Testing untuk "+className+" SELESAI ... "+timeWithSecond.format(now)+"\n");
		  System.out.println("=== HASIL AKHIR ===");
		  System.out.println("Skenario "+className+" = "+testcase_result+reason+"\n");
    }

}
