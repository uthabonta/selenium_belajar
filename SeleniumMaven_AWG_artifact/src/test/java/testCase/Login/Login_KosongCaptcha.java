package testCase.Login;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class Login_KosongCaptcha {
	
	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
//	String webdriverPath = "/home/adew/UTHA/selenium/geckodriver-v0.25.0-linux64/geckodriver";
	String url = "https://grosir.axisnet.id";
	public static WebDriver driver;
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	String user = "jendraljoko";
	String pass = "Password*10";
	String divLoginError = "";
	
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
    LocalDateTime now = LocalDateTime.now();
    
    ChromeOptions options = new ChromeOptions();
    public WebElement element;
    
    String title = null;
    String urlNow = null;
    String testcase_result = null;
    String messageWrongInput = null;
    String random = RandomStringUtils.randomAlphanumeric(5);
	
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver",webdriverPath);
	  driver = new ChromeDriver();
	  
  }
  
  
  @BeforeMethod
  public void getCurrentLoc() {
	  title = driver.getTitle();
	  urlNow = driver.getCurrentUrl();
	  //System.out.println("=====  Anda berada di page = "+title+" dengan url = "+urlNow+" ======");
  }

  
  @Test(priority = 0)
  public void gotoUrl() {
	  driver.get(url);
	  
  }
  
  @Test(priority = 1)
  public void inputUsername() {
	  driver.findElement(By.name("username")).sendKeys(user);
	  
  }
  @Test(priority = 2)
  public void inputPassword() {
	  driver.findElement(By.name("password")).sendKeys(pass);
	  
  }
  @Test(priority = 3)
  public void checkBox() {
	  driver.findElement(By.id("disclaimer")).click();
	  
  }
  
  @Test(priority = 4)
  public void Captcha() throws InterruptedException {
	  //Thread.sleep(7000);
	  driver.findElement(By.id("login-captcha")).sendKeys("");
	  
  }
  
  @Test(priority = 5)
  public void klikSubmit() {
	  driver.findElement(By.id("btn_submit")).click();
	  
  }
  
  @AfterMethod
  public void afterMethod(ITestResult result) {
    System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
  }

  

  @AfterTest
  public void validasiAfterTest() {
	  System.out.println("");
	  System.out.println("=== HASIL AKHIR ===");
	  String expectedUrl = "https://grosir.axisnet.id/index";
	  urlNow = driver.getCurrentUrl();
	  
	  try {
		  messageWrongInput = driver.findElement(By.id("msg_p")).getText();
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println("Tidak ada error");
	}
	  
	  
	  
	  if (urlNow.equals(expectedUrl)) {
		  testcase_result = "SUKSES";
		  System.out.println("Login "+testcase_result);
	}else {
		testcase_result = "GAGAL";
		System.out.println("Login "+testcase_result);
		System.out.println("Error output frontend : "+messageWrongInput);
		System.out.println("");
		
	}
  }
  

}
