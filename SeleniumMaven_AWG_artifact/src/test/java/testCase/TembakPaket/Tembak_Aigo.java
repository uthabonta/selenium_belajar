package testCase.TembakPaket;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Login_OneClass;

public class Tembak_Aigo {
	
	String className = this.getClass().getSimpleName();
	String dirResources	= "/home/adew/git/selenium_belajar/SeleniumMaven_AWG_artifact/resources/";
	String fileUploadAigo = "tembak1.xlsx"; 
	String bucketId = null;
	
	String testcase_result = null;
	int int_testcase_result = 0;
	String reason = null;
	
	public static WebDriver driver = null;
	String webdriverPath = "/home/adew/UTHA/selenium/chromedriver_linux64/chromedriver";
	ChromeOptions options = new ChromeOptions();
	
	DateTimeFormatter timeWithSecond = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
	
    DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    
	@BeforeTest
	  public void LoginAWG() throws InterruptedException{
		  System.setProperty("webdriver.chrome.driver",webdriverPath);
		  options.addArguments("--start-maximized");
		  driver = new ChromeDriver(options);
		  Login_OneClass loginAWGFirst = new Login_OneClass();
		  System.out.println("Automation Testing untuk "+className+" DIMULAI ... "+timeWithSecond.format(now)+"\n");
		  loginAWGFirst.loginStep(driver);
	  }
	
	 @BeforeMethod
	  public void beforeMethod() throws InterruptedException{
		  Thread.sleep(1000);
	  }
	
	@Test(priority = 1)
	public void menuTembak() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.id("tembak-tab")).click();
	}
	
	@Test(priority = 2)
	public void klikOpsiPaket() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.id("sel3")).click();

	}
	
	@Test(priority = 3)
	public void pilihPaket() throws Exception {
//		Thread.sleep(1000);
		Select pilihPaket = new Select(driver.findElement(By.xpath("//select[@id='sel3']")));
		List<WebElement> listPilihPaket = pilihPaket.getOptions();
		int jmlPaket = listPilihPaket.size();
		
//		pilihPaket.selectByIndex(53);
		pilihPaket.selectByVisibleText("AIGO - Nelpon Sepuasnya ke sesama AXIS 14hr Exp by 22 Nov 2019 (1) -- Beli Rp 1");
//		AIGO - Nelpon Sepuasnya ke sesama AXIS 14hr Exp by 31 Oct 2019 (6) -- Beli Rp 1
		
		System.out.println("getoptions = "+listPilihPaket);
		System.out.println("sizing paket = "+jmlPaket);
	}
	
	
	@Test(priority = 3)
	public void klikRadioButton() throws Exception {
		
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement klikRadioButton = driver.findElement(By.xpath(" //div[@class='anchor tem-pv col-md-6']//input[@name='tembak_type']"));
		executor.executeScript("arguments[0].click();", klikRadioButton);
	}
	
	@Test (priority = 4)
	public void uploadFile() throws Exception {
		WebElement upload = driver.findElement(By.xpath("//input[@name='sapi_terbang']"));
		upload.sendKeys(dirResources+fileUploadAigo);
	}
	
	@Test(priority = 5)
	public void browseFile() throws Exception {
		driver.findElement(By.xpath("//button[contains(text(),'Tembak Paket')]")).click();
	}
	
	@Test(priority = 6)
	public void waitForOTP() throws InterruptedException {
		System.out.println("Sedang mengisi OTP & CAPTCHA");
		Thread.sleep(10000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Test(priority = 7)
	public void klikVerifikasi() throws Exception {
		driver.findElement(By.id("btn_otp_verify")).click();
	}
	
	@Test(priority = 8)
	public void getIdTransaksi() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement getId = driver.findElement(By.id("msg_notif"));
		String idTrx = wait.until(ExpectedConditions.visibilityOf(getId)).getText();
		
		System.out.println("idTransaksi anda = "+idTrx);
		
		Tembak_Aigo sa = new Tembak_Aigo();
			sa.getTextAfter(idTrx);
	}
	
	//method trim trxId
  	public void getTextAfter(String text) {
			String [] input = text.split(" ");
			int lastIndex = input.length;
			String lastText=(input[lastIndex-1]);
			bucketId = lastText.substring(1, 8);
			System.out.println("BucketId = "+bucketId);
			
		}
	
	@Test(priority = 9)
	public void klikOk() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement getOk = driver.findElement(By.id("btn_notif_ok"));
		wait.until(ExpectedConditions.visibilityOf(getOk));
		getOk.click();
	}
	
//	@Test(priority = 10)
//	public void validasiInfoTrx_openTab() throws Exception {
//  		System.out.println("\n"+"=== PROSES VALIDASI INFO TRANSAKSI ===");
//		driver.findElement(By.id("transaksi-tab")).click();
//	}
//
//	@Test(priority = 11)
//	public void validasiInfoTrx_startDate() {
//		//driver.findElement(By.id("date")).sendKeys(date.format(now));
//		driver.findElement(By.id("date")).sendKeys(date.format(now));
//	}
//  
//  
//	@Test(priority = 12)
//	public void validasiInfoTrx_endDate() {
//		driver.findElement(By.id("date2")).sendKeys(date.format(now));
//	}
//  
//	
//	
//	@Test(priority = 13)
//	public void pilihTipeTrx() throws Exception {
////		Thread.sleep(1000);
//		Select pilihTipe = new Select(driver.findElement(By.xpath("//select[@id='transaksiInfo']")));
//		List<WebElement> listTipe = pilihTipe.getOptions();
//		int jmlTipe = listTipe.size();
//		
////		pilihPaket.selectByIndex(53);
//		pilihTipe.selectByVisibleText("Physical Voucher");
////		AIGO - Nelpon Sepuasnya ke sesama AXIS 14hr Exp by 31 Oct 2019 (6) -- Beli Rp 1
//		
//		System.out.println("getoptions = "+listTipe);
//		System.out.println("sizing paket = "+jmlTipe);
//	}
//	
//	@Test(priority = 14)
//	public void validasiInfoTrx_submitDate() {
//		driver.findElement(By.id("form_transaksi")).click();
//	}
//	
//	@Test (priority = 15)
//	public void validasi_searchBucketId() throws InterruptedException{
//		Thread.sleep(5000);
//		driver.findElement(By.xpath("//input[@type='search']")).sendKeys(bucketId);
//	}
//	
//	@Test(priority = 16)
//	public void getAllData() throws Exception {
//		Thread.sleep(2000);
//		String idTrans = driver.findElement(By.xpath("//td[3]")).getText();
//		System.out.println("ID TRANSAKSI = "+idTrans);
//		WebElement status = driver.findElement(By.xpath("//td[5]"));
//		System.out.println("Status  = "+status.getText());
//		
//		
//		String filename = driver.findElement(By.xpath("//td[8]")).getText();
//		System.out.println("Filename  = "+filename);
//		String hargaBeli = driver.findElement(By.xpath("//td[7]")).getText();
//		System.out.println("Harga beli = "+hargaBeli);
//		
//		WebElement bucketId = driver.findElement(By.xpath("//td[5]"));
//		System.out.println("Status  = "+bucketId.getText());
//		
//		
//		
//	}
// 	id btn_otp_verify klik	//button name verifikasi
//	id msg_notif -- utk get id trx
//	id btn_notif_ok ////button name Ok
//	
//	id transaksiInfo/ name typeTrans
//	select Physical Voucher
//	klik button search 
// 	search by idTRx --> //div[@id='table-transaksi_filter']//input[@type='search']
//	div[@id='table-transaksi_filter']//input[@class='form-control input-sm'] atau type="search"	
//	get //td[3] -- get id trx
//	get //td[5] -- get status	
//	get //td[8] -- get filename
// 	td[7] -- get harga beli
//	td[10] -- get bucketId
			
	;
	@AfterMethod
    public void afterMethod(ITestResult result) {
      System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
    }
	
	@AfterTest
    public void validasiAfterTest() {
  		System.out.println("");
  		if (int_testcase_result == 2) {
  			testcase_result = "SUKSES";
		}else {
			testcase_result = "GAGAL";
		}
  		
  		
  		System.out.println(int_testcase_result);
  		System.out.println("Automation Testing untuk "+className+" SELESAI ... "+timeWithSecond.format(now)+"\n");
		  System.out.println("=== HASIL AKHIR ===");
		  System.out.println("Skenario "+className+" = "+testcase_result+reason+"\n");
    }
}
