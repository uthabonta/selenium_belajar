package validasi.infoTransaksi;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

public class CekBeli_InfTrx {
	
	String namaPaketInfoTrx = null;
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
    LocalDateTime now = LocalDateTime.now();
    
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println(">> Proses Validasi di Info Transaksi <<");
  }
  
  @Test(priority = 0)
	public void validasiInfoTrx_openTab(WebDriver driver) throws Exception {
		driver.findElement(By.id("transaksi-tab")).click();
	}
//div[@id='transaksi']//tr[1]//td[8]
	@Test(priority = 1)
	public void validasiInfoTrx_startDate(WebDriver driver) {
	driver.findElement(By.id("date")).sendKeys(dtf.format(now));	
	}
  
  
  @Test(priority = 2)
  public void validasiInfoTrx_endDate(WebDriver driver) {
	driver.findElement(By.id("date2")).sendKeys(dtf.format(now));
  }
  
  @Test(priority = 3)
  public void validasiInfoTrx_submitDate(WebDriver driver) {
	driver.findElement(By.id("form_transaksi")).click();
	}
  
  @Test(priority = 4)
  public void validasiInfoTrx_getData(WebDriver driver) {
	WebElement bucketId =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
	System.out.println("BucketId = "+bucketId.getText());
	namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]")).getText();
	System.out.println("Nama Paket di Info Transaksi = "+namaPaketInfoTrx);
	}
  

  @AfterMethod
  public void afterMethod(ITestResult result) {
    System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
  }

}
