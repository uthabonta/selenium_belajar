package validasi.infoTransaksi;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import testCase.Beli.Beli_PulsaCukup;

public class CekBuy_InfTrx extends Beli_PulsaCukup{
	
	
	
	DateTimeFormatter date = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	LocalDateTime now = LocalDateTime.now();
	String testcase_result = null;
	
	public void validasi (){
		System.out.println("=== PROSES VALIDASI INFO TRANSAKSI ===");
		driver.findElement(By.id("transaksi-tab")).click();
		driver.findElement(By.name("date")).click();
		driver.findElement(By.id("date")).sendKeys(date.format(now));
		System.out.println("haahaha");
		
		driver.findElement(By.name("startDate")).sendKeys(date.format(now));
		//	
		driver.findElement(By.id("date2")).sendKeys(date.format(now));
		driver.findElement(By.id("form_transaksi")).click();
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//ambil bucketID
		  WebElement bucketId =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[8]"));
		bucketId = wait.until(ExpectedConditions.visibilityOf(bucketId));
		System.out.println("BucketId = "+bucketId.getText());
		//ambil nama paket
		WebElement namaPaketInfoTrx =  driver.findElement(By.xpath("//div[@id='transaksi']//tr[1]//td[3]"));
		namaPaketInfoTrx = wait.until(ExpectedConditions.visibilityOf(namaPaketInfoTrx));
		System.out.println("Nama Paket di Info Transaksi = "+namaPaketInfoTrx.getText());
		}
	}
