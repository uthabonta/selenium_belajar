package validasi.stok;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import common.GlobalProperties;
import common.Login_Suite;
import common.Validasi_InfoTrx;

public class Stok_CekStokOut extends Login_Suite{
	String expiredDate = null;
	
	
	@Test (priority = 6)
	public void bukaMenuStok() {
		System.out.println("===CEK STOK===");
		driver.findElement(By.id("stok-tab")).click();
	}
	
	@Test (priority = 7)
	public void searchBucketId() throws InterruptedException{
		Validasi_InfoTrx bucket =  new Validasi_InfoTrx();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//input[@type='search']")).sendKeys(bucket.bucketId2);
	}
	
	@Test (priority = 8)
	public void getExpiredDate() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		expiredDate = driver.findElement(By.xpath("//td[4]")).getText().replace(" ", "-");
		//expiredDate = new StringBuilder(expiredDate).insert(expiredDate.length()-4, "-").toString();
		
		int sisaStokSaatIni = Integer.valueOf(driver.findElement(By.xpath("//td[6]")).getText());
		System.out.println("Sisa stok = "+sisaStokSaatIni);
		System.out.println("Expired date = "+expiredDate);
	}

}
