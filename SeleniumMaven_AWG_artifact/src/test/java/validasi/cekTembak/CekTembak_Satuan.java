package validasi.cekTembak;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CekTembak_Satuan {
	
	public static WebDriver driver;
	
	
	@BeforeMethod
	public void beforeStep () {
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}
	
	@Test(priority = 1)
	public void klikTab() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement tabCekTembak = driver.findElement(By.id("check-tab"));
		wait.until(ExpectedConditions.visibilityOf(tabCekTembak));
		tabCekTembak.click();
	}
	
	@Test(priority = 2)
	public void klikRadio() throws Exception {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		WebElement klikRadioButton = driver.findElement(By.xpath("//div[@id='check']//input[@class='singnochk']"));
		executor.executeScript("arguments[0].click();", klikRadioButton);
	}
	
	@AfterMethod
	  public void afterMethod(ITestResult result) {
	    System.out.println("Anda baru saja melakukan step :" + result.getMethod().getMethodName());
	  }


}
